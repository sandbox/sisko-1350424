<?php

function simplechat_form($form_state){

  if( is_moderator() ){
    $url = explode('/', $_REQUEST['q']);
    if( sizeof($url) > 1 )
      $chatNode = node_load( $url[1] );
    }else{		
      $chatNode = node_load( $_SESSION['simplechat']['CHATNID'] );
    }
  /*drupal_set_message("Checking chatNID: {$chatNode->nid} <br> 
  Name: {$chatNode->field_guestname[0]['value']} <br> 
  EMail: {$chatNode->field_guestmail[0]['value']} <br> 
  Phone: {$chatNode->field_guestphone[0]['value']}"
  );*/

  /**************************
  * check if the user is
  * 'registered' otherwise
  * redirect to the register
  * page
  ***************************/
  if( isset($chatNode->field_guestname[0]['value']) && isset($chatNode->field_guestmail[0]['value']) && isset($chatNode->field_guestphone[0]['value']) ){
  }else{
    #drupal_set_message('VARIABLES NOT SET!');
    unset( $_SESSION['simplechat']['CHATNID'] );
  }

  $path = drupal_get_path('module', 'simplechat');
  drupal_add_js($path .'/js/simplechat.main.js', 'module', 'header');

  #$url	=	arg(0);
  #drupal_set_message("Testing : {$_SESSION['simplechat']['CHATNID']}");
  #print_r($_SESSION);

  $node_types = node_get_types('names');
  $roles = user_roles();

  global $user;
  $prefix = $user->name ? "Chatting with - {$chatNode->field_guestname[0]['value']}" : "Hello, {$chatNode->field_guestname[0]['value']}";

  $url = explode('/', $_REQUEST['q']);
  if( sizeof($url) > 1 || !is_moderator() ){
    $form = array();
    $form['simplechat_messages'] = array(
      '#type' => 'textarea',
      '#title' => t(''),
      '#description' => t(''),
      '#weight' => -1,
      '#prefix' => $prefix,
    );
    $form['simplechat_message'] = array(
      '#title' => t('Type your message'),
      '#type' => 'textfield',
      '#description' => t(''),
      '#weight' => 0,
    );
    $form['chatnid'] = array(
      '#type' => 'hidden',
      '#value' => $chatNode->nid,
    );

    if( is_moderator() ){
      $form['simplechat_status']['status'] = array(
      '#type' => 'select',
      '#title' => t('Status'),
      '#default_value' => variable_get('simplechat#'.$chatNode->nid.'_status','open'),
      '#options' => array(
        'open' => t('open'),
        'closed' => t('closed'),
      ),
      );
    }

    $form['submit_button'] = array(
      '#type' => 'submit',
      '#value' => 'send',
    );

  return $form;
  }
}
	
function simplechat_form_submit($form_id, &$form_state) {

  if( is_moderator() ){
    $url = explode('/', $_REQUEST['q']);
    if( sizeof($url) > 1 )
      $chatNode = node_load( $url[1] );
    }else{
      $chatNode = node_load( $_SESSION['simplechat']['CHATNID'] );
    }

  drupal_set_message("Message Recieved from : {$chatNode->field_guestname[0]['value']}");

  if( $chatNode ){
    global $user;
    $name = $user->name ? $user->name : $chatNode->field_guestname[0]['value'];
    $chatNode->body .= "\n{$name} : {$form_state['values']['simplechat_message']}";
    $chatNode->field_chat_status[0]['value'] = strtoupper($form_state['values']['status']);
    node_save($chatNode);
  }else{
    unset( $_SESSION['simplechat']['CHATNID'] );
  }
}
	
/***********************
* registering the
* theme hook for
* the simple chat form
***********************/
function simplechat_theme() {
  return array(
    'simplechat_form' => array(
      'arguments' => array('form' => NULL)
    )
  );
}
	
/**************************
* the actual modification
* to the simplechat form
**************************/
function theme_simplechat_form($form) {

  global $user;
  $chatROLE = variable_get('simplechat_role', 0);
  if( is_moderator() ){
    $nodes = node_load_by_type('chat');

    $list = "<ul class='present-users'>";
    foreach( $nodes as $node ){
      $chatNode = node_load($node->nid);
      if($chatNode->field_chat_status[0]['value'] == 'CLOSED' || $chatNode->field_chat_status[0]['value'] == NULL)
        $status = "CLOSED";
      else
        $status = "OPEN";
        
      $list .= "<li> <a href='/simplechat/{$node->nid}' target='_blank'> {$node->title} </a>  [{$status}] </li>";
    }
    $list	.=	"</ul>";

    $output = '';
    $output .= drupal_render($form);
    $output .= "<div class='guests'> {$list} </div>";
    return $output;
  }else{
    return;
  }
}
	
function node_load_by_type($type, $limit = 15, $offset = 0) {
  $nodes = array();
  $query = db_rewrite_sql("SELECT nid FROM {node} n WHERE type = '%s'", 'n');
  $results = db_query_range($query, $type, $offset, $limit);

  while($nid = db_result($results)) {
    $nodes[] = node_load($nid);
  }
  return $nodes;
}
