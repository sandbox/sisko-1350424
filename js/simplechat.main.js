$(document).ready(
	function(){
	
		$chat		=	'';
		$chatNID	=	$('input#edit-chatnid').val();
		//alert($chatNID);
	
		$path	= window.location;
		/*$parts	=	$path.split('/');
		$nid	=	$parts[2];*/
		$baseroot	=	Drupal.settings.simplechat.baseroot;
		//alert( "working on : " + $baseroot );

		Drupal.settings.basePath;
		$path	=	 Drupal.settings.basePath+'sites/all/modules/simplechat/simplechat.core.php?function=UPDATECHAT&nid='+$chatNID;
		if( $('textarea#edit-simplechat-messages').length > 0 ){
  		loadChat($path);
  	}

		/***************
		 * re-load chat
		 * at intervals
		 ***************/
		setInterval(function() {
			if( $('textarea#edit-simplechat-messages').length > 0 ){
    		loadChat($path);
    	}
		}, 2000);

		function loadChat($path){

			$.ajax({
				url: $path,
				asych: false,
				success: function($data){
				$('textarea#edit-simplechat-messages').html( $data ).scrollTop(99999).attr('readonly','readonly');
				if( $chat == '' ){
					$chat	=	$data;
				}else if( $chat != $data ){
					//alert('content has changed!');
					$('form#simplechat-form').append('<embed id="sound">');
              				$('#sound').html("<embed src='"+$baseroot+"/sites/all/modules/simplechat/doorbell.wav' hidden='true' autostart='true' loop='false'>");
					$chat	=	$data;
				}
			},
			error: function(){
					$('textarea#edit-simplechat-messages').text( "loading ...!" );
				}
			});
		}//close loadChat
		
		/***************************
		 * making sure a message is
		 * supplied before allowing
		 * submission
		 ****************************/
		 	$('button#edit-submit-button').click(
		 		function(e){
		 			if( $('input#edit-simplechat-message').text() == '' ){
		 				e.preventDefault();
		 				alert('Please enter a message!');
		 			}
		 		}
		 	);
		
	}	
);
