$(document).ready(
	function(){
		
		$link	=	Drupal.settings.simplechat.link;
		//alert('Testing : ' + $link);
		
		/***********************
		 * dynamically enable
		 * configured link to
		 * load chat popout
		 ***********************/
		 if( $link != '' ){
			  $($link).click(
				  function(e){
					  e.preventDefault();
					  window.open ("/simplechat", "SimpleChat","status=0,toolbar=0, height=500, width=500");
				  }
			  );
		 }
		
		/***********************
		 * periodically reload
		 * users list
		 ***********************/
		 	//loadUsers();
			/*function loadUsers(){
				Drupal.settings.basePath;
				$listpath	=	 Drupal.settings.basePath+'sites/all/modules/simplechat/simplechat.core.php?function=USERSLIST';
				listUsers($listpath);
			}*/
			Drupal.settings.basePath;
			$listpath	=	 Drupal.settings.basePath+'sites/all/modules/simplechat/simplechat.core.php?function=USERSLIST';

			/***************
			 * re-load chat
			 * at intervals
			 ***************/
			setInterval(function() {
				listUsers($listpath);
			}, 2000);

			function listUsers($listpath){

				$.ajax({
					url: $listpath,
					asych: false,
					success: function($data){
						$('div#block-simplechat-0 div.content').html( $data );
				},
				error: function(){
						$('div#block-simplechat-0 div.content').text( "loading ...!" );
					}
				});
			}//close loadUsers

	}
);
