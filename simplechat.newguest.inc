<?php

function simplechat_newcomer_form($form_state){
  $form = array();
  $form['simplechat_newcomer']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('name'),
    '#description' => t(''),
    '#weight' => -1,
  );
  $form['simplechat_newcomer']['email'] = array(
    '#title' => t('email'),
    '#type' => 'textfield',
    '#description' => t(''),
    '#weight' => 0,
  );
  $form['simplechat_newcomer']['phone'] = array(
    '#title' => t('telephone No.'),
    '#type' => 'textfield',
    '#description' => t(''),
    '#weight' => 0,
  );
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => 'enter',
  );

  return $form;
}
	
function simplechat_newcomer_form_submit($form_id, &$form_state){

  #drupal_set_message("NAME : {$form_state['values']['name']}!");
  #drupal_set_message("EMAIL: {$form_state['values']['email']}!");
  #drupal_set_message("PHONE: {$form_state['values']['phone']}!");

  $chatNode = new stdClass();
  $chatNode->type = 'chat';
  $chatNode->title = "Chatting with {$form_state['values']['name']}";

  //cck fields
  $chatNode->field_guestname[0]['value'] = $form_state['values']['name'];
  $chatNode->field_guestmail[0]['value'] = $form_state['values']['email'];
  $chatNode->field_guestphone[0]['value'] = $form_state['values']['phone'];

  node_save($chatNode);

  $_SESSION['simplechat']['CHATNID'] = $chatNode->nid;
  #drupal_set_message("SESSION Variable setting : {$_SESSION['simplechat']['CHATNID']}");
}
